package inheritance;

public class Player {
	private static int idGenerator = 101;
	private int id;
	private String name;
	
	public Player(){
		
	}
	public Player(String name){
		this.name = name;
		id = idGenerator++;
	}
	public void setName(String name){
		this.name = name;
	}
	public String getName(){
		return name;
	}
	public int getId(){
		return id;
	}
	public String toString(){
		return "id = "+id+",name = " +name;
	}

}
