package inheritance;

public class Batman extends Player {
	
		private int runsScored;
		private int centuries;
		private int halfCenturies;
		private int ballsFaced ;
		private int sixes;
		private int fours;


		public Batman(String name,int runsScored, int centuries, int halfCenturies, int ballsFaced, int sixes, int fours) {
		super(name);
		this.runsScored = runsScored;
		this.centuries = centuries;
		this.halfCenturies = halfCenturies;
		this.ballsFaced = ballsFaced;
		this.sixes = sixes;
		this.fours = fours;
		}


		public int getRunsScored() {
		return runsScored;
		}
	}
