package inheritance;

public class Bowler extends Player {

			private int ballsBowled;
			private int runsLeaked;
			private int wickets;


			public Bowler(String name,int ballsBowled, int runsLeaked, int wickets) {
			super(name);
			this.ballsBowled = ballsBowled;
			this.runsLeaked = runsLeaked;
			this.wickets = wickets;
			}
			public int getBallsBowled() {
			return ballsBowled;
			}
			public void setBallsBowled(int ballsBowled) {
			this.ballsBowled = ballsBowled;
			}
			public int getRunsLeaked() {
			return runsLeaked;
			}
			public void setRunsLeaked(int runsLeaked) {
			this.runsLeaked = runsLeaked;
			}
			public int getWickets() {
			return wickets;
			}
			public void setWickets(int wickets) {
			this.wickets = wickets;
			}
			public float getStrikeRate() {
			return ( ballsBowled / wickets);

			}

			public String toString() {
			return "Bowler ["+super.toString()+"ballsBowled=" + ballsBowled + ", runsLeaked=" + runsLeaked + ", wickets=" + wickets
			+ ", getBallsBowled()=" + getBallsBowled() + ", getRunsLeaked()=" + getRunsLeaked() + ", getWickets()="
			+ getWickets() + ", getStrikeRate()=" + getStrikeRate() + "]";
			}

			
	

}
