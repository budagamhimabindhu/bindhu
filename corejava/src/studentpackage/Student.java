package studentpackage;

public class Student{
	private int SId;
	private String Name;
	private long PhNo;
	private String S_class;
	public void setSId(int SId){
		this.SId = SId;
	}
	public void setName(String Name){
		this.Name = Name;
	}
	public void setPhNo(long PhNo){
		this.PhNo = PhNo;
	}
	public void setS_class(String S_class){
		this.S_class = S_class;
	}
	public int getSId(){
		return SId;
	}
	public String getName(){
		return Name;
	}
	public long getPhNo(){
		return PhNo;
	}
    public String getS_class(){
	return S_class;
}
	
	
}