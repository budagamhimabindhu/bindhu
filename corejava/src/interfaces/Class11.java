package interfaces;

public class Class11 extends Student{
private float  phy_theory;
private float phy_practical;
private float chm_theory;
private float chm_practical;
private float math_theory;
private float math_practical;
private float english;
private float hindi;


	

public Class11(String name,float phy_theory,float phy_practical,float chm_theory,float chm_practical,float math_theory,float math_practical,float english,float hindi){
	super(name);
	this.phy_theory=phy_theory;
	this.phy_practical=phy_practical;
	this.chm_theory=chm_theory;
	this.chm_practical=chm_practical;
	this.math_theory=math_theory;
	this.math_practical=math_practical;
	this.english=english;
	this.hindi=hindi;
}
public void setPhy_theory(float  phy_theory){
	this.phy_theory=phy_theory;
}

public float getPhy_theory(){
	return phy_theory;
}
public void setPhy_practical(float phy_practical){
	this.phy_practical=phy_practical;
}
public float getPhy_practical(){
	return phy_practical;
}
public void setChm_theory(float chm_theory){
	this.chm_theory=chm_theory;
}
public float getChm_theory(){
	return chm_theory;
}
public void setChm_practical(float chm_practical){
	this.chm_practical=chm_practical;
}
public float getChm_practical(){
	return chm_practical;
}
public void setMath_practical(float math_practical){
	this.math_practical=math_practical;
}
public float getMath_practical(){
	return math_practical;
}
public void setMath_theory(float math_theory){
	this.math_theory=math_theory;
}
public float getMath_theory(){
	return math_theory;
}
public void setEnglish(float english){
	this.english=english;
}
public float getEnglish(){
	return english;
}
public void setHindi(float hindi){
	this.hindi=hindi;
}
public float getHindi(){
	return hindi;
}
public float findPercentage(){
	float total=phy_theory+phy_practical+chm_theory+chm_practical+math_theory+math_practical;
	float percentage=(total*100)/5;
	return percentage;
}
public String toString(){
	return " name = " +getName()+ " phy_theory = " +phy_theory+ " phy_practical = " +phy_practical+ " chm_theory = " +chm_theory+ " chm_practical = " +chm_practical+ " math_theory = " +math_theory+ " math_practical = " +math_practical+ " percentage = " +findPercentage()+ " grade = " +(grade(findPercentage()));
}
}
