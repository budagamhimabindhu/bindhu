package interfaces;

public class Class8 extends Student{
	private float phy;
	private float chm;
	private float math;
	private float english;
	private float hindi;
	 
	public Class8(){
		
	}
	public Class8(String name,float phy,float chm,float math,float english,float hindi){
		super(name);
		this.phy=phy;
		this.chm=chm;
		this.math=math;
		this.english=english;
		this.hindi=hindi;
	}
	
	public void setPhy(float phy){
		this.phy=phy;
	}
	public float getPhy(){
		return phy;
	}
	public void setChm(float chm){
		this.chm=chm;
	}
	public float getChm(){
		return chm;
	}
	public void setMath(float math){
		this.math=math;
	}
	public float getMath(){
		return math;
	}
	public void setEnglish(float english){
		this.english=english;
	}
	public float getEnglish(){
		return english;
	}
	public void setHindi(float hindi){
		this.hindi=hindi;
	}
	public float getHindi(){
		return hindi;
	}
	public float findPercentage() {
		float  total=phy+chm+math+english+hindi;
		float Percentage=(total*100)/5;
		return Percentage;
	}
	public String toString(){
		return " name= " +getName()+ " phy = " +phy+ " chm = " +chm+ " math = " +math+ " english = " +english+ " hindi = " +hindi+ " percentage = " +findPercentage()+ " grade = " +(grade(findPercentage()));
	}
}
