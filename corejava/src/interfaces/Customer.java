package interfaces;

public class Customer implements Bank {
	private double investment;
	private double tenure;
	public Customer(double investment,double tenure){
		this.investment=investment;
		this.tenure=tenure;
	}
	public double getInvestment(){
		return investment;
	}
	public void setInvestment(double investment){
		this.investment=investment;
	}
	public double getTenure(){
		return tenure;
	}
	public void setTenure(double tenure){
		this.tenure=tenure;
	}
	public String toString(){
		return "Customer[ investment = " +investment+ "tenure = " +tenure+ "]";
		
	}
	@Override
	public double calROI() {
		// TODO Auto-generated method stub
		return 0;
	}
	

}
