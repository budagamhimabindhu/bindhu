package interfaces;

public class SBI implements Bank{
private double rateOfInterest;
private Customer customer;

public SBI(Customer customer){
	this.customer=customer;
}
public Customer getCustomer(){
	return customer;
}
public void setCustomer(Customer customer){
	this.customer=customer;
}
public double calcROI(){
	rateOfInterest=(customer.getTenure()/customer.getInvestment())*100;
	return rateOfInterest;
}
public String toString(){
	return "SBI[ customer = "+customer+ " rateOfInterest = " +calcROI()+ "]";
}
@Override
public double calROI() {
	// TODO Auto-generated method stub
	return 0;
}
}
