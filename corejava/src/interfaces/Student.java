package interfaces;

abstract class Student {
	private static int idGenerator=101;
	private int id;
	private String name;
public Student(){
		
	}
	public Student(String name){
		this.name = name;
		id = idGenerator++;
	}
public void setName(){
	this.name=name;
}
public String getName(){
	return name;
}
public int getId(){
	return id;
}
public String grade(double percentage){
	if(percentage>90)
		return "A+";
	else if(percentage>70 && percentage<90)
		return "A";
	else if(percentage>65 && percentage<70)
		return "B";
	else 
		return "c";
}

public abstract float findPercentage();
	

public String toString(){
	return "Student[ id="+id+ "name ="+name+"]" ;
}
}
