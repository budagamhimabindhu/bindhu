package employee;

public class Employee {
	private static int idGenerator = 102;
	private int id;
	private String firstName;
	private String lastName;
	private int salary;
	
	Employee(String FirstName,String LastName,int Salary){
		this.id = idGenerator++;
		this.firstName = FirstName;
		this.lastName = LastName;
		this.salary = Salary;
	}
	public int getId(){
		return id;
	}
	public String getName(){
		return firstName+" "+lastName;
	}
	public int getAnnualSalary(){
		return salary*12;
	}
	public int getraiseSalary(int percent){
		int raiseSalaryPercent = (salary*percent)/100;
		int raiseSalary = salary + raiseSalaryPercent;
		return raiseSalary;
	}
	
	public String toString(){
		return "Employee[id = "+id+ ",Name ="+getName()+",salary ="+salary+",AnnualSalary = " +getAnnualSalary() +"]";
	}

}
