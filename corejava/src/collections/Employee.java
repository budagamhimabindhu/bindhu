package collections;
 public class Employee implements Comparable<Employee> {
	private static int idGenerator = 101;
	private int id;
	private String name;
	private double salary;
	
	public Employee(String name,double salary){
		id =  idGenerator++;
		this.name = name;
		this.salary =  salary;
	}

public void setName(){
	this.name = name;
}
public String getName(){
	return name;
}
public void setSalary(){
	this.salary = salary;
}
public double getSalary(){
	return salary;
}
public String toString(){
	return "Employee [ id = " +id+ " name = " +getName()+ " salary = " +salary+ "]";
}


public int CompareTo(Employee e){

	if(salary > e.getSalary())
		return 1;
	else if(salary < e.getSalary())
		return -1;
	return 0;
}

@Override
public int compareTo(Employee o) {
	// TODO Auto-generated method stub
	return 0;
}
}