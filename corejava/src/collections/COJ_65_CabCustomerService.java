package collections;
import java.util.ArrayList;
import java.util.List;

public class COJ_65_CabCustomerService {
	
	private List<COJ_65_CabCustomer>customerList = new ArrayList<COJ_65_CabCustomer>();
		
	public void addCabCustomer(COJ_65_CabCustomer customer){
		customerList.add(customer);
		
		
	}
	public boolean isFirstCustomer(COJ_65_CabCustomer customer){
		for(COJ_65_CabCustomer c : customerList){
			if(c.getPhone()==customer.getPhone())
				return false;
		}
		
		return true;
	}
	public double calculateBill(COJ_65_CabCustomer customer){
		if(isFirstCustomer(customer))
			return 0.0;
		else if(customer.getDistance() <= 4)
			return 80.0;
		else
			return(80 + (6 * customer.getDistance() - 4));
	}
		public String printBill(COJ_65_CabCustomer customer){
			return customer.getCustomerName() + "please pay your bill of rs." +calculateBill(customer);
		}
		
	}


