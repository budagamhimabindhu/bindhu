package collections;

import java.util.ArrayList;
import java.util.Collections;

public class ComparatorUsage {

		public static void main(String[] args) {
		
			Employee emp1 = new Employee("ankit");
			Employee emp2 = new Employee("brad");
		
			ArrayList<Employee> list = new ArrayList<Employee>();
		
			list.add(emp1);
			list.add(emp2);

			Collections.sort(list, new Employee2.ComparatorName());
			System.out.println(list);

		}
	}
}
