package collections;

	import java.util.Comparator;

	public class PublisherComparator implements Comparator <Book1>{

	@Override
	public int compare(Book1 b1, Book1 b2) {
	 
	return b1.getPublisher().compareTo(b2.getPublisher());
	 
	}
	}
	

