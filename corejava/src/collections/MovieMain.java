package collections;

import java.util.List;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;



public class MovieMain {
	public static void main(String args[]){

		

		Movie m1 = new Movie("Bhahubali","Rajamouli",2,4.5f);
		Movie m2 = new Movie("Siva","Aaju",3,3.4f);
		Movie m3 = new Movie("Maharshi","Vamshi",4,4.2f);
	
		List<Movie>list1 = new ArrayList<Movie>();
		   list1.add(m1);
		   list1.add(m2);
		   list1.add(m3);
		   
		   for(Movie m : list1){
			   System.out.println(m);
		   }
Collections.sort(list1,new NameComparator());
System.out.println("========================");
System.out.println("Name sorting");
for(Movie m : list1){
	System.out.println(m);
}
Collections.sort(list1,new DurationComparator());
System.out.println("==========================");
System.out.println("Duration sorting");
for(Movie m : list1){
System.out.println(m);	
}
	}
}
