package collections;

public class Book {
	private static int idGenerator = 102;
	private int id;
	private String name;
	private String author;
	
	public Book(String name,String author){
		this.id = idGenerator++;
		this.name=name;
		this.author=author;
	}
	public int getId(){
		return id;
	}
	public void setName(String name){
		this.name = name;
	}
	public String getName(){
		return name;
	}
	public void setAuthor(String author){
		this.author=author;
	}
	public String getAuthor(){
		return author;
	}
	public String toString(){
		return " Book [id = " +id+ " name = " +name+ " author = " +author+ " ] ";
	}

}
