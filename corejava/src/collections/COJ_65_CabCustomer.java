package collections;

public class COJ_65_CabCustomer {
	
private int customerId; 
private String customerName ; 
private String pickupLocation;
private String dropLocation;
private int distance ;
private long phone;
private static int  customerIdGenerator=1000;
public COJ_65_CabCustomer (String customerName, String pickupLocation, 
String dropLocation, int distance, long phone) {
super();
customerId=customerIdGenerator++;

this.customerName = customerName;
this.pickupLocation = pickupLocation;
this.dropLocation = dropLocation;
this.distance = distance;
this.phone = phone;
}
public String getCustomerName() {
return customerName;
}
public void setCustomerName(String customerName) {
this.customerName = customerName;
}
public String getPickupLocation() {
return pickupLocation;
}
public void setPickupLocation(String pickupLocation) {
this.pickupLocation = pickupLocation;
}
public String getDropLocation() {
return dropLocation;
}
public void setDropLocation(String dropLocation) {
this.dropLocation = dropLocation;
}
public int getDistance() {
return distance;
}
public void setDistance(int distance) {
this.distance = distance;
}
public long getPhone() {
return phone;
}
public void setPhone(long phone) {
this.phone = phone;
}

public String toString() {
return "CabCustomer [customerId=" + customerId + ", customerName=" + customerName + ", pickupLocation=" + pickupLocation
+ ", dropLocation=" + dropLocation + ", distance=" + distance + ", phone=" + phone + ", customerIdGenerator="
+ customerIdGenerator + "]";
}
}

