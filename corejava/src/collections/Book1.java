package collections;

public class Book1 {
	
	private int id;
	private String name;
	private String author;
	private String Publisher;
	private static int Idgenerator=101;
	 
	 
	public Book1(String name, String author, String publisher) {
	id=Idgenerator++;
	this.name = name;
	this.author = author;
	this.Publisher = publisher;
	}


	public int getId() {
	return id;
	}


	public void setId(int id) {
	this.id = id;
	}


	public String getName() {
	return name;
	}


	public void setName(String name) {
	this.name = name;
	}


	public String getAuthor() {
	return author;
	}


	public void setAuthor(String author) {
	this.author = author;
	}


	public String getPublisher() {
	return Publisher;
	}


	public void setPublisher(String publisher) {
	Publisher = publisher;
	}


	@Override
	public String toString() {
	return "Book1 [id=" + id + ", name=" + name + ", author=" + author + ", Publisher=" + Publisher + "]";
	}
	 
	}

	

	