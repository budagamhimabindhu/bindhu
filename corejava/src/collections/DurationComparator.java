package collections;

import java.util.Comparator;

public class DurationComparator implements Comparator<Movie> {
	public int compare(Movie m1,Movie m2){
		if(m1.getDuration()> m2.getDuration())
			return 1;
		else if(m1.getDuration()< m2.getDuration())
			return -1;
		return 0;
	}

}
