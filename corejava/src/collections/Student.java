package collections;

public class Student {
 private static int idGenerator=101;
 private int id;
 private String name;
 private int subject1;
 private int subject2;
 private int subject3;
 
 public Student(int id,String name,int subject1,int subject2,int subject3){
	 id = idGenerator++;
	 this.name=name;
	 this.subject1=subject1;
	 this.subject2=subject2;
	 this.subject3=subject3;
 }
 public Student(){
		
	}
	
		
	public void setName(){
		this.name=name;
	}
	public String getName(){
		return name;
	}
	public int getId(){
		return id;
	}
	public void setSubject1(){
		this.subject1=subject1;
	}
	public int getSubject1(){
		return subject1;
	}
	public void setSubject2(){
		this.subject2=subject2;
	}
	public int getSubject2(){
		return subject2;
	}
	public void setSubject3(){
		this.subject3=subject3;
	}
	public int getSubject3(){
		return subject3;
	}
	public float findPercentage() {
		float  total=subject1+subject2+subject3;
		float Percentage=(total*100)/3;
		return Percentage;
	}
 public String toString(){
	 return " Student1 [Id = " +id+ " name = "+getName()+ " subject1 = " +subject1+ " subject2 = "+subject2+ " subject3 = "+subject3+ " percentage = " +findPercentage()+"]";
 }
}
