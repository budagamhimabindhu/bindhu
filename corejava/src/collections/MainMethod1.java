package collections;


import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;

public class MainMethod1 {
	
	
		public  void getEmployeeList(){
		
		Employee e1=new Employee("bindhu",30000);
		Employee e2=new Employee("hima",40000);
		Employee e3=new Employee("nandhu",50000);
		Employee e4=new Employee("shashi",10000);
		Employee e5=new Employee("renu",15000);
		Employee e6=new Employee("shiva",25000);
		
		ArrayList<Employee>EmployeeList = new ArrayList<Employee>();
		EmployeeList.add(e1);
		EmployeeList.add(e2);
		EmployeeList.add(e3);
		EmployeeList.add(e4);
		EmployeeList.add(e5);
		EmployeeList.add(e6);
		
		System.out.println("EmployeeList");
		for(Employee e :EmployeeList){
		System.out.println(e);
		
		}
		System.out.println("=====================");
		System.out.println("After sort");
		Collections.sort(EmployeeList);
		for(Employee e :EmployeeList){
			System.out.println(e);
		}
		
		ArrayList<Employee> FreshersList = new ArrayList<Employee>();
		
		Iterator<Employee> it = EmployeeList.iterator();
		
		while(it.hasNext()){
			
			Employee e = it.next();
			if(e.getSalary()< 20000){
			FreshersList.add(e);
			it.remove();
			}
		}
		
		System.out.println("================================");
		System.out.println("FreshersList");
		for(Employee e : FreshersList){
			System.out.println(e);
		}
		Collections.sort(FreshersList);
		System.out.println("");
		System.out.println("After Sorting FreshersList");
		for(Employee e : FreshersList){
			System.out.println(e);
		}
		
}
}
