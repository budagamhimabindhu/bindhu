package Exception;

import java.util.Scanner;

public class VoterEligibility {

	public static void main(String[] args) throws InvalidAgeException{
		Scanner sc = new Scanner(System.in);
		System.out.println("enter age : ");
		int age = sc.nextInt();
		
		if(age<18)
			throw new InvalidAgeException("Age must be greater than 18");
		
		
	}

}
