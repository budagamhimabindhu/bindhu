package Exception;

import java.util.List;

public class COJ_67_Hospital {
	
	private static int id=1000;
	private String HospitalName;
	private int HospitalCode;
	private List<String> listoftreatment;
	private String contactName;
	private  String  contactNum;
	private String Location;
	public COJ_67_Hospital(String hospitalName, List<String> listoftreatment, String contactName,
	String contactNum, String location) {
	super();
	HospitalCode=id++;
	HospitalName = hospitalName;
	//this.id = id;
	this.listoftreatment = listoftreatment;
	this.contactName = contactName;
	this.contactNum = contactNum;
	Location = location;
	}
	public int getHospitalCode() {
	return HospitalCode;
	}
	public void setHospitalCode(int hospitalCode) {
	HospitalCode = hospitalCode;
	}
	public String getHospitalName() {
	return HospitalName;
	}
	public void setHospitalName(String hospitalName) {
	HospitalName = hospitalName;
	}
	public List<String> getListoftreatment() {
	return listoftreatment;
	}
	public void setListoftreatment(List<String> listoftreatment) {
	this.listoftreatment = listoftreatment;
	}
	public String getContactName() {
	return contactName;
	}
	public void setContactName(String contactName) {
	this.contactName = contactName;
	}
	public String getContactNum() {
	return contactNum;
	}
	public void setContactNum(String contactNum) {
	this.contactNum = contactNum;
	}
	public String getLocation() {
	return Location;
	}
	public void setLocation(String location) {
	Location = location;
	}
	@Override
	public String toString() {
	return "COJ_67_Hospital [HospitalCode=" + HospitalCode + ", HospitalName=" + HospitalName + ", id=" + id
	+ ", listoftreatment=" + listoftreatment + ", contactName=" + contactName + ", contactNum=" + contactNum
	+ ", Location=" + Location + "]";
	}

}
