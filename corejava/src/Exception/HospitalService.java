package Exception;

	import java.util.ArrayList;
	import java.util.HashMap;
	import java.util.List;
	import java.util.Map;

	public class HospitalService extends HospitalMain {
	List<COJ_67_Hospital> lh = new ArrayList<COJ_67_Hospital>();

	public int addHospital(COJ_67_Hospital h1) {
	lh.add(h1);
	return h1.getHospitalCode();
	}

	public Map<Integer,String> getHospitals() 
	{
	   Map<Integer,String> m = new HashMap<Integer,String>();
	   for( COJ_67_Hospital h : lh) {
	  
	  m.put(h.getHospitalCode(), h.getHospitalName());
	   }
	return m; 
	}
	public COJ_67_Hospital getHospitalDetails(int search) {
	for(COJ_67_Hospital hos : lh) {
	if(hos.getHospitalCode()==search) 

	return hos;


	}
	return null;
	}
	}



