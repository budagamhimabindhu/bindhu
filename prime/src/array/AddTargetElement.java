package array;

import java.util.HashSet;
import java.util.Scanner;

public class AddTargetElement {
	public static void main(String[] args) {

		initializeArray();
	}

	public static void initializeArray() {

		Scanner sc = new Scanner(System.in);

		System.out.println("Enter size of the array");

		int size = sc.nextInt();

		int arr[] = new int[size];

		System.out.println("Enter elements of the array");

		for (int i = 0; i < size; i++) {

			arr[i] = sc.nextInt();

		}
		System.out.println("enter target value");
		int k = sc.nextInt();
		if(getTargetElement(arr, k)){
			System.out.println("true");
		}
		else
			System.out.println("false");
	}

	public static boolean getTargetElement(int[] arr, int k) {
		
			HashSet<Integer> s = new HashSet<Integer>();
			for (int i = 0; i < arr.length; ++i) {

				int temp = k - arr[i];

				if (s.contains(temp)) {
				return true;
				}
					s.add(arr[i]);
				}
			
			return false;
		}

	}

