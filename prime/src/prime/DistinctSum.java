package prime;

import java.util.HashSet;

public class DistinctSum {
	public static void main(String args[]) {
		int arr[] = { 1, 2, 2, 1, 3, 4, 3, 5 };
		System.out.println(distinctSum(arr));
	
	}

	public static int distinctSum(int[] arr) {
		int sum = 0;
		HashSet<Integer> s = new HashSet<Integer>();
		for (int num:arr) {
			s.add(num);
			
		}
		for(int i:s){
			sum=sum+i;
		}
		
		return sum;
	}
}
