package anagrams;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Sawpods {

	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub
		Map<String, List<String>> hm = new HashMap();
		List<String> anagrams;
		FileReader fr = new FileReader("/home/user/Downloads/sowpods.txt");
		BufferedReader br = new BufferedReader(fr);
		String word;
		while ((word = br.readLine()) != null) {
			String sw = sort(word);

			if (hm.containsKey(sw)) {
				hm.put(sw, hm.get(sw)).add(word);
			}

			else {
				anagrams = new ArrayList();
				anagrams.add(word);
				hm.put(sw, anagrams);
			}

		}
		
		for(Map.Entry<String, List<String>> entry : hm.entrySet() ){
			if(entry.getValue().size() > 1)
				System.out.println(entry.getValue());
		}
		
	}

	public static String sort(String word) {
		String sortedWord;
		char[] tempArray = word.toCharArray();
		Arrays.sort(tempArray);
		sortedWord = new String(tempArray);
		return sortedWord;

	}
}