package strings;

import java.util.Scanner;

public class ReversePallindrome {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		String s = sc.nextLine();
		System.out.println(isPallindrome(s));
	}


public static boolean isPallindrome(String s){
	String revstring = "";
	for(int i = s.length()-1; i >= 0; i--){
		revstring += s.charAt(i);
		
	}
	System.out.println(revstring);
	return(s.equalsIgnoreCase(revstring));
}
}