package strings;


import java.util.Scanner;

public class ReverseString1 {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.println("enter string :");
		String str = sc.nextLine();
		System.out.println(getReverse(str));
		
	}

	public static String getReverse(String str) {
		
		String revstr = "";
		String s[] = str.split(" ");
		
		for(int i=0; i<s.length;i++){
			String word = s[i];
			
			
		for(int j=word.length()-1;j>=0;j--){
			revstr += word.charAt(j);
		}
		revstr += " ";
	}
      return revstr;
	}
}
