package strings;

import java.util.Scanner;

public class StringInReverseOrder {

	public static void main(String[] args) {
		Scanner sc=new Scanner(System.in);
		String s=sc.nextLine();
		String s1=sc.next();
		
		System.out.println(" nextLine() : " +s );
		
		System.out.println(" next() : " +s1 );
		
		System.out.println(" Length : " +s.length());
		
		System.out.println(" char At : " +s.charAt(3));
		
		System.out.println(" indexOf : " +s.indexOf('e'));
		
		System.out.println(" LastIndexOf : " +s.lastIndexOf('l'));
		
		System.out.println(" Uppercase : " +s.toUpperCase());
		
		System.out.println(s);
		
		System.out.println(" Equals : " +s.equals(s1));
		
		System.out.println(" EqualsIgnoreCase : " +s.equalsIgnoreCase(s1));
		
		System.out.println(" SubString : " +s.substring(0,4));
		System.out.println("s :" +s);
		System.out.println("s1 :" +s1);
		s=s.concat(s1);
		
	}

}
