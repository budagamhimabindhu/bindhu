package strings;

import java.util.Scanner;

public class TextCaseChanging {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		String str = sc.nextLine();
		System.out.println(changeCase(str));
		

	}

	private static String changeCase(String str) {
		String convertCase = "";
		
		for(int i=0;i<str.length();i++){
			char c = str.charAt(i);
			
			
			if(Character.isLowerCase(c))
				convertCase += Character.toUpperCase(c);
			
			
			else if(Character.isUpperCase(c))
				convertCase += Character.toLowerCase(c);
			
			
			else
				convertCase += c;
				
		}
	
		return convertCase;
	}

}
