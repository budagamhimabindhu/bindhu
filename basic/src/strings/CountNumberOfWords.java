package strings;

import java.util.Scanner;

public class CountNumberOfWords {

	public static void main(String[] args) {
      Scanner sc = new Scanner(System.in);
		
		String str = sc.nextLine();
		
		
		
      System.out.println(countNumberOfWords(str));

	}

	private static int countNumberOfWords(String str) {
		
		int count =0;
		
		String split[]=str.split(" ");
		
		for(int i=0;i<split.length;i++){
			if(!split[i].isEmpty()){
				count++;
			}
		}
		
		return count;
		}
}
	


