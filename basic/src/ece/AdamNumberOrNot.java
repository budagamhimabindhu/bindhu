package ece;

public class AdamNumberOrNot {
	    
	    static int reverseDigits(int num) 
	    { 
	        int rev = 0; 
	        while (num > 0) 
	        { 
	            rev = rev * 10 + num % 10; 
	            num /= 10; 
	        } 
	        return rev; 
	    } 
	  
	     
	    static int square(int num) 
	    { 
	        return (num * num); 
	    } 
	  
	    
	    static boolean checkAdamNumber(int num) 
	    { 
	        // Square first number and square 
	        // reverse digits of second number  
	        int a = square(num); 
	        int b = square(reverseDigits(num)); 
	          
	        // If reverse of b equals a then given 
	        // number is Adam number 
	        if (a == reverseDigits(b)) 
	        return true; 
	        return false;         
	    } 
	  
	     
	    public static void main(String[] args) 
	    { 
	        int num = 12; 
	          
	        if (checkAdamNumber(num)) 
	        System.out.println("Adam Number"); 
	        else
	        System.out.println("Not a Adam Number");     
	    } 
	  

}
