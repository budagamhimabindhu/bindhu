package files;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.IOException;

public class FileInputStreamDemo {
	
	public static void main(String[] args) throws IOException{
		FileInputStream input = new FileInputStream("bindhu.txt");
		BufferedInputStream buffer = new BufferedInputStream(input);
		int c;
		while(( c = input.read()) != -1){
			System.out.print((char)c);
		}
	}

}
