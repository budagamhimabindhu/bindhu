package files;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

public class FileWriteDemo {

	public static void main(String[] args)throws IOException {
		FileWriter writer = new FileWriter("bindhu.txt");
		String s = "this is a string";
		char c = 'c';
		
		BufferedWriter bufferwriter = new BufferedWriter(writer);
		bufferwriter.write(s);
		bufferwriter.close();
		
	}

}
