package files;

import java.io.File;
import java.io.IOException;

public class Files {
	public static void main(String[] args){
		File f = new File("new.txt");
		try{
			if(f.createNewFile())
				System.out.println("File created successfully........");
			else
				System.out.println("File not created...........");
			System.out.println(f.getAbsolutePath());
		}
		catch(IOException e){
			e.printStackTrace();
		}
	}

}
