package files.student;

import java.io.FileWriter;
import java.io.IOException;

public class StudentFileHandling {

	

	public static void main(String[] args)throws IOException{
		Student s1 =  new Student("nandu",78,87,76);
		Student s2 = new Student("kiran",67,45,76);
		
		FileWriter writer = new FileWriter("Student.csv");
		writer.write("Id,Name,m1,m2,m3,Percentage\n");
		writer.write(s1.toString());
		writer.write(s2.toString());
		writer.close();
		
	}
}
