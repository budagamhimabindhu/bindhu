package introduction;

public class demo {	
	

	public static int factOf(int x) {
		if (x == 1)
			return 1;
		else
			return x * factOf(x - 1);

	}

	public static void main(String[] args) {
		int res = factOf(5);
		System.out.println(res);
	}
}