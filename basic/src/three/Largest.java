package three;

import java.util.Scanner;

public class Largest {
	public static void main(String[] args){
		int num1,num2,num3;
		
		Scanner sc=new Scanner(System.in);
		num1=sc.nextInt();
		num2=sc.nextInt();
		num3=sc.nextInt();
		System.out.println(Greatest(num1,num2,num3)+ " is greatest number");
	}

	private static int Greatest(int num1, int num2, int num3) {
		int res;
		if(num1>num2 && num1>num3){
			res=num1;
			return res;
		}
		else if(num2>num1 && num2>num3){
			res=num2;
			return res;
		}
		else if(num3>num1 && num3>num2){
			res=num3;
			return res;
		}
		return 0;		
	}

}
