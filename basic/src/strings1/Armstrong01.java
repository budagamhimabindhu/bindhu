package strings1;

public class Armstrong01 {
	public static void main(String[] args) {
		int num1 = 100;
		int num2 = 125;
		System.out.println(generateArmstrongNums(num1, num2));
	}

	public static String generateArmstrongNums(int start, int limit) {
		String result = "";
		if (start <= 0 || limit <= 0)
			return "-1";
		else if (start >= limit)
			return "-2";
		else {
			for (int i = start; i < limit; i++) {
				if (isArmstrong(i))
					result = result + i + ",";
			}
		}
		if(result.isEmpty())
			return "-3";
		return result.substring(0, result.length()-1);
		// ADD YOUR CODE HERE
	}

	public static boolean isArmstrong(int num) {
    	return num == sumOfPowersOfDigits(num);
    }

	

	//private static int SumOfPowersOfDigits(int num) {
		// TODO Auto-generated method stub
		//return 0;
	//}

	public static int sumOfPowersOfDigits(int n) {
    	int temp = n,digitcount = 0,sum = 0;
    	
    	int digits[]= getDigits(n);
    	for(int i = 0; i < digits.length; i++){
    		int powervalue = power(digits[i],digits.length);
    		sum += powervalue;
    	}
    	return sum;
    	}
    

	public static int[] getDigits(int n) {
		int temp = n, digitcount = 0;
		
		while(temp >  0){
    		digitcount++;
    		temp /= 10;
		}
		int digits[] = new int[digitcount];
		int i = 0;
		
		while (n > 0) {
			
			int digit = n % 10;
			digits[i] = digit;
			i++;
			n /= 10;
		}
		return digits;
	}

	public static int power(int d, int p) {
		double pow = Math.pow(d, p);
		int power = (int) pow;

		return power;
		
	}
}
