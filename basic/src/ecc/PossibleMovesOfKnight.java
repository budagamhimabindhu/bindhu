package ecc;

import java.util.Scanner;

public class PossibleMovesOfKnight {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);

		int currentPosition[] = new int[2];

		System.out.println("Give current position ");

		for (int i = 0; i < 2; i++) {

			currentPosition[i] = sc.nextInt();

		}
		/*
		 * System.out.println("enter x position :"); int x = sc.nextInt();
		 * System.out.println("enter y position :"); int y = sc.nextInt();
		 */
		System.out.println("Possible Moves are " + getPossibleMovesOfKnight(currentPosition));

	}

	public static int getPossibleMovesOfKnight(int[] currentPosition) {
		int[][] moves = { { -1, 2 }, { 1, 2 }, { 2, -1 }, { 2, 1 }, { -2, 1 }, { -2, -1 }, { 1, -2 }, { -1, -2 } };

		int count = 0;
		int p, q;
		for (int[] move : moves) {
			p = currentPosition[0] + move[0];
			q = currentPosition[1] + move[1];

			if (p >= 0 && q >= 0 && p < 8 && q < 8) {

				count++;
			}

		}
		return count;
	}
}