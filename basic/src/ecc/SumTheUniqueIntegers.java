package ecc;

public class SumTheUniqueIntegers {

	public static void main(String[] args) {
		int arr[] = { 4, 5, 6, 6, 5, 3, 1, 9 };

		getSumOfUnique(arr);

	}

	private static void getSumOfUnique(int[] arr) {
		int sum = 0;
		for (int i = 0; i < arr.length; i++) {
			int count = 0;

			for (int j = 0; j < arr.length; j++) {
				if (arr[i] == arr[j])
					count++;
			}
			if (count == 1)
				sum += arr[i];
		}
		System.out.println(sum);

	}

}
