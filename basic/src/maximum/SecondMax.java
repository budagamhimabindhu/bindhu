package maximum;
import java.util.Scanner;

public class SecondMax {

	public static void main(String[] args) {
		Scanner sc=new Scanner(System.in);
		int n=sc.nextInt();
		System.out.println(getSecondMax(n));
	}
	public static int getSecondMax(int n){
		int digit=0;
		int max=0,SecondMax=0;
		for(;n>0;n/=10){
			digit=n%10;
			
			if(digit>max){
				SecondMax=max;
				max=digit;
			}
			else if(digit<max && digit>SecondMax)
				SecondMax=digit;
		}
		// TODO Auto-generated method stub
   return SecondMax;
	}

}
