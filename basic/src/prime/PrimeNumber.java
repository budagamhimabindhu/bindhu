package prime;

import java.util.Scanner;

public class PrimeNumber {

	public static void main(String[] args) {
		Scanner sc=new Scanner(System.in);
		int num;
		num=sc.nextInt();
		if(getPrimeNumber(num))
			System.out.println(num + "is a prime number");
		else
		System.out.println(num + "is not a prime number");
	}
	private static boolean getPrimeNumber(int num){
		
		int i;
		for(i=2;i<num;i++){
			if(num%i==0)
				return false;
			}
				
		
       return true;
	}

}
