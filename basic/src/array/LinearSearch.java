package array;

import java.util.Scanner;
public class LinearSearch {

	public static void main(String[] args) {
		Scanner sc=new Scanner(System.in);
		System.out.println("enter array size :");
		int size=sc.nextInt();
		int arr[]=new int[size];
		System.out.println("enter array elements :");
		for(int i=0;i<size;i++){
			arr[i]=sc.nextInt();
		}
		System.out.println("enter search element :");
		int se=sc.nextInt();
		int index=Search(arr,se);
		if(index!=-1)
			System.out.println("element found :" +index);
		else
			System.out.println("element not found :");
		// TODO Auto-generated method stub

	}
public static int Search(int arr[],int se){
	for(int i=0;i<arr.length;i++){
		if(arr[i]==se)
			return i;
	}
	return -1;
	}
}

