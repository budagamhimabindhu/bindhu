package number1;

import java.util.Scanner;

public class Digit {

	public static void main(String[] args) {
		Scanner sc=new Scanner(System.in);
		int num;
		
		num=sc.nextInt();
		System.out.println(getDigit(num));
	}
	private static int getDigit(int num){
		int rem=0;
		while(num>0){
			rem=num%10;
			System.out.println(" Digit " +rem);
			num=num/10;
			
		}
		return rem;
		
	}

}
