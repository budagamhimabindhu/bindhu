package file1;

public class Student1 {
	private String Name;
	private  int id;
	private static int idGenerator=100;
	private  int m1;
	private  int m2;
	private  int m3;
	
	public  Student1(String Name,int m1,int m2,int m3){
		super();
		this.id=idGenerator++;
		this.Name=Name;
		this.m1=m1;
		this.m2=m2;
		this.m3=m3;
	}
	
	public void setName(String Name){
		this.Name=Name;
	}
	public String getName(){
		return Name;
	}
	public void setM1(int m1){
		this.m1=m1;
	}
	public int getM1(){
		return m1;
	}
	public void setM2(int m2){
		this.m2=m2;
	}
	public int getM2(){
		return m2;
	}
	public void setM3(int m3){
		this.m3=m3;
	}
	public int getM3(){
		return m3;
	}
	public double getPercentage(){
		return (m1+m2+m3)/3;
				}
	public String toString(){
		return  id+ ","+Name+ ","  +m1+ "," +m2+ "," +m3+", "+getPercentage()+ "\n";
	}
}



