package integer;

import java.util.Scanner;

public class AverageValue {

	public static void main(String[] args) {
		
		 System.out.println("average : " +getAverage());
	}

	private static int getAverage() {
		int a[]=new int[5];
		int sum=0;
		Scanner sc=new Scanner(System.in); 
		System.out.println("enter array inputs :");
		
		for(int i=0;i<5;i++){
			a[i]=sc.nextInt();
			sum += a[i];
			
		}
		return (sum/5);
	}

}

