package sellingprice;

import java.util.Scanner;
public class Discount {
	public static void main(String[] args){
		Scanner sc=new Scanner(System.in);
		float price=sc.nextInt();
		
		float SellingPrice=getSellingPrice(price);
		System.out.println("Selling price after Discount :"+SellingPrice);
	}

	private static float getDiscount(float price) {
		float discount=0.0f;
		if(price>0 && price<10000)
			discount=(price*10)/100;
		else if(price>10000 && price<20000)
			discount=(price*20)/100;
		else
			discount=(price*25)/100;
		return discount;
		// TODO Auto-generated method stub
		

	}
	public static float getSellingPrice(float price){
		float discount=getDiscount(price);
		return price-discount;
	}

}
