package reverse;
import java.util.Scanner;
public class ReverseNumber {
	public static void main(String[] args){
		
		Scanner sc=new Scanner(System.in);
		int n=sc.nextInt();
	
		System.out.println("given number :" + n);
		System.out.println("reverse number:" + getReverse(n));
	}
	public static int getReverse(int n){
	int rem=0,rev=0;
	while(n>0){
		rem=n%10;
		rev=rev*10+rem;
		n /=10;
	}
	return rev;
	}

}
