
package pallindrome;
import java.util.Scanner;

public class ThreeDigitPallindrome {

	public static void main(String[] args) {
		Scanner sc=new Scanner(System.in);
		int num;
		num=sc.nextInt();
		System.out.println(getThreeDigitPallindrome(num));
		// TODO Auto-generated method stub

	}

	private static int getThreeDigitPallindrome(int num) {
		
		int rem=0,rev=0;
		while(num>0){
			rem=num%10;
			rev=rev*10+rem;
			num /=10;
		}
		if(num%10==num/10){
			return 1;
		}
		else{
			return 0;
			
		}
			
	}

}
