import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class CSVExample {

	public static void main(String[] args) throws IOException{
		 FileWriter w = new FileWriter("SpreadSheet.csv");
		 w.write("Sno,Name,Address\n");
		 w.write("1,priya,hyd\n");
		 w.write("2,anu,mumbai\n");
		 w.write("3,renu,wgl");
		 w.close();
		 
		 FileReader r = new FileReader("SpreadSheet.csv");
		 int c;
		 
		 while((c = r.read()) != -1){
			 System.out.print((char)c);
		 }
		 

	}

}
